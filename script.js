'use strict';

import Graph from './graph.js';
import GraphDraw from './graphDraw.js';

const n1 = 3;
const n2 = 2;
const n3 = 0;
const n4 = 5;
let k = 1.0 - n3 * 0.01 - n4 * 0.005 - 0.05;

const canvas = document.getElementById('canvas');
const canvas2 = document.getElementById('canvas2');
const context = canvas.getContext('2d');

canvas.height = window.innerHeight;
canvas.width = window.innerWidth;

const nodeRadius = 30;
const distanceFromCenter = 350;
const amountOfNodes = 10 + n3;
const generator = new Math.seedrandom([n1, n2, n3, n4].join);

let graph = new Graph(amountOfNodes);
let path = [];
let graphDraw;
let counter = 0;
let pathMatrix = new Array(amountOfNodes);
for (let i = 0; i < amountOfNodes; i++) {
  pathMatrix[i] = new Array(amountOfNodes).fill(0);
}

graph.randm(generator);
graph.mulmr(k);
graph.toUndirected();
graphDraw = new GraphDraw(canvas, graph, nodeRadius);
graph.generateWeightMatrix(generator);
graphDraw.graphDrawTriangle(distanceFromCenter);
console.log(graph.adjacencyMatrix);
console.log(graph.weightMatrix);
console.log(graph.primMST(path));

document.getElementById('reset').addEventListener('click', () => {
  console.clear();
  context.clearRect(0, 0, canvas.width, canvas.height);
  counter = 0;
  path = [];
  graphDraw.graphDrawTriangle(distanceFromCenter);
  console.log(graph.adjacencyMatrix);
  console.log(graph.weightMatrix);
  console.log(graph.primMST(path));
});
document.getElementById('next').addEventListener('click', () => {
  if (counter < path.length) {
    const node1 = path[counter].node1;
    const node2 = path[counter].node2;
    graphDraw.drawPath(node1, node2);
    counter++;
  }
});
