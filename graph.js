'use strict';
export default class Graph {
  constructor(numberNodes) {
    this.numberNodes = numberNodes;
    this.adjacencyMatrix = [];
    this.isDirected = true;
    for (let i = 0; i < this.numberNodes; i++) {
      this.adjacencyMatrix[i] = new Array(this.numberNodes).fill(0);
    }
  }
  randm(generator) {
    for (const arr of this.adjacencyMatrix) {
      for (let i = 0; i < arr.length; i++) {
        arr[i] = generator() * 2;
      }
    }
  }
  mulmr(k) {
    for (const arr of this.adjacencyMatrix) {
      for (let i = 0; i < arr.length; i++) {
        arr[i] = arr[i] * k >= 1 ? 1 : 0;
      }
    }
  }
  toUndirected() {
    const matrix = this.adjacencyMatrix;
    const oldMatrix = [];
    for (let i = 0; i < matrix.length; i++) {
      oldMatrix[i] = matrix[i].slice();
    }
    for (let i = 0; i < matrix.length; i++) {
      for (let j = 0; j < matrix.length; j++) {
        if (matrix[i][j] === 1) matrix[j][i] = matrix[i][j];
      }
    }
    this.isDirected = !this.isDirected;
    return oldMatrix;
  }
  generateWeightMatrix(generator) {
    const weightMatrix = [];
    for (let i = 0; i < this.numberNodes; i++) {
      weightMatrix[i] = new Array(this.numberNodes).fill(0);
    }
    const b = [];
    for (let i = 0; i < this.numberNodes; i++) {
      b[i] = new Array(this.numberNodes).fill(0);
      for (let j = 0; j < this.numberNodes; j++) {
        b[i][j] = generator() * 2;
      }
    }
    const c = [];
    for (let i = 0; i < this.numberNodes; i++) {
      c[i] = new Array(this.numberNodes).fill(0);
      for (let j = 0; j < this.numberNodes; j++) {
        c[i][j] = Math.ceil(b[i][j] * 100 * this.adjacencyMatrix[i][j]);
      }
    }
    const d = [];
    for (let i = 0; i < this.numberNodes; i++) {
      d[i] = new Array(this.numberNodes).fill(0);
      for (let j = 0; j < this.numberNodes; j++) {
        d[i][j] = c[i][j] === 0 ? 0 : 1;
      }
    }
    const h = [];
    const tr = [];
    for (let i = 0; i < this.numberNodes; i++) {
      h[i] = new Array(this.numberNodes).fill(0);
      tr[i] = new Array(this.numberNodes).fill(0);
      for (let j = 0; j < this.numberNodes; j++) {
        h[i][j] = d[i][j] !== d[j][i] ? 1 : 0;
        tr[i][j] = i < j ? 1 : 0;
        weightMatrix[i][j] = (d[i][j] + h[i][j] * tr[i][j]) * c[i][j];
        weightMatrix[j][i] = weightMatrix[i][j];
      }
    }
    this.weightMatrix = weightMatrix;
    return weightMatrix;
  }
  primMST(path) {
    let sum = 0;
    const weightMatrix = this.weightMatrix;
    let currNode = 0;
    const visited = [];
    while (visited.length < this.numberNodes - 1) {
      if (visited.includes(currNode)) {
        continue;
      }
      visited.push(currNode);
      let minEdge = {
        value: 999,
        node1: 0,
        node2: 0
      };
      for (let i of visited) {
        for (let j = 0; j < this.numberNodes; j++) {
          if (i !== j && weightMatrix[i][j] < minEdge.value && weightMatrix[i][j] !== 0 && !visited.includes(j)) {
            minEdge.value = weightMatrix[i][j];
            minEdge.node1 = i;
            minEdge.node2 = j;
          }
        }
      }
      path.push(minEdge);
      sum += minEdge.value;
      currNode = minEdge.node2;
    }
    return { path, sum };
  }
  addEdge(node1, node2) {
    if (this.isDirected) {
      this.adjacencyMatrix[node1][node2] = 1;
    } else {
      this.adjacencyMatrix[node1][node2] = 1;
      this.adjacencyMatrix[node2][node1] = 1;
    }
  }
  deleteEdge(node1, node2) {
    if (this.isDirected) {
      this.adjacencyMatrix[node1][node2] = 0;
    } else {
      this.adjacencyMatrix[node1][node2] = 0;
      this.adjacencyMatrix[node2][node1] = 0;
    }
  }
}
